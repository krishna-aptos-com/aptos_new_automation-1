import {LogInPageObjects} from "../../pages/webuipages/logInPageObjects";
import {EJViewerPageObjects} from "../../pages/webuipages/ejviewerPageObjects";

// const got = require('got');
const assert = require('assert');

// const tunnel = require('tunnel');

const { Given, Then, When} = require('cucumber');
// const chai = require('chai').use(require('chai-as-promised'));
// const expect = chai.expect;

Given(/^User launch APTOS "([^"]*)" and navigated to APTOS Login Screen$/, async (URL) => {
    browser.url(URL);
    console.log('APTOS Home Screen is displayed successfully');
    await browser.pause(5000);
    LogInPageObjects.loginLink.click();
    console.log('Able to launch the Login screen successfully');
    await browser.pause(5000);
});

When(/^User enter "([^"]*)" and "([^"]*)"$/, async (userName, password) => {
    LogInPageObjects.userNameTextbox.waitForExist(6000);
    LogInPageObjects.userNameTextbox.setValue(userName);
    LogInPageObjects.passwordTextbox.setValue(password);
    LogInPageObjects.logInButton.click();

});

Then(/^User navigate to EJViewer and select "([^"]*)" and Enter "([^"]*)"$/, async (store , deviceId) => {
    EJViewerPageObjects.storeTextbox.waitForExist(6000);
    // assert.strictEqual(EJViewerPageObjects.ejViewerLabel.getText().trim(),'Aptos Denim - EJ Viewer','EJ Viewer page is displayed ');
    await browser.pause(5000);
    await EJViewerPageObjects.storeTextbox.setValue(store);
    await browser.pause(5000);
    browser.keys('Enter');
    await browser.pause(4000);
    browser.keys('Tab');
    browser.keys('Enter');
    await browser.pause(4000);
    console.log('selected ' + store + ' from the store list' + 'and deviceId ' + deviceId);
});

Then(/^User validate the “(.*)”$/, async (errorMsg) => {
    console.log('Actual Error Message for Invalid credentials : '+ LogInPageObjects.loginErrorLabel.getText());
    console.log('Expected Error Message for Invalid Credentials: ' + errorMsg)
    // @ts-ignore
    assert.strictEqual(LogInPageObjects.loginErrorLabel.getText().trim(),errorMsg.trim(),'Invalid Login credentil error ');
    await browser.pause(3000);
});
