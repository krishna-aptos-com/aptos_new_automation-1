
const argv = require('yargs').argv;
const chai = require('chai');


exports.config = {
    
    port: '4444',
    //path: '/',
    // ...
   // services: ['chromedriver'],

   
 //  services: ['selenium-standalone'],
   // chromeDriverArgs: ['--port=9999'],
    //chromeDriverLogs: './',
    //
    // ==================
    // Specify Test Files
    // ==================
    // Define which test specs should run. The pattern is relative to the directory
    // from which `wdio` was called. Notice that, if you are calling `wdio` from an
    // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working
    // directory is where your package.json resides, so `wdio` will be called from there.
    //
    specs: [
        './features/testurl.feature'
    ],
  reporters: ['multiple-cucumber-html'],
    reporterOptions: {
        htmlReporter: {
            removeFolders: true,
            jsonFolder: './report',
            reportFolder: './report/webbrowser',
            // ... other options, see Options
            displayDuration: true,
            openReportInBrowser: true,
            saveCollectedJSON: true,
            disableLog: true,
            pageTitle: 'APTOS WEB UI AUTOMATION',
            reportName: 'aptos_webui_automation',
            pageFooter: '<div><h1>WEB BROWSER AUTOMATION</h1></div>',
            customData: false,
            customData: {
                title: 'Web UI Run info',
                data: [
                    {label: 'Project', value: 'APTOS'},
                    {label: 'Release', value: '1.2.3'},
                    {label: 'Cycle', value: 'B11221.34321'},
                    {label: 'Execution Start Time', value: 'Nov 19th 2017, 02:31 PM EST'},
                    {label: 'Execution End Time', value: 'Nov 19th 2017, 02:56 PM EST'}
                ]
            },
    }
    },

    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
    //
    // ============
    // Capabilities
    // ============
    // Define your capabilities here. WebdriverIO can run multiple capabilities at the same
    // time. Depending on the number of capabilities, WebdriverIO launches several test
    // sessions. Within your capabilities you can overwrite the spec and exclude options in
    // order to group specific specs to a specific capability.
    //
    // First, you can define how many instances should be started at the same time. Let's
    // say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have
    // set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec
    // files and you set maxInstances to 10, all spec files will get tested at the same time
    // and 30 processes will get spawned. The property handles how many capabilities
    // from the same test should run tests.
    //
    maxInstances: 1,
    //
    // If you have trouble getting all important capabilities together, check out the
    // Sauce Labs platform configurator - a great tool to configure your capabilities:
    // https://docs.saucelabs.com/reference/platforms-configurator
    //
   

    capabilities: [
        {
            maxInstances: 1,
            browserName: 'chrome',
            // Add this
        }
    ],
    //
    // ===================
    // Test Configurations
    // ===================
    // Define all options that are relevant for the WebdriverIO instance here
    //
    // By default WebdriverIO commands are executed in a synchronous way using
    // the wdio-sync package. If you still want to run your tests in an async way
    // e.g. using promises you can set the sync option to false.
    sync: true,
    //
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'verbose',
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Warns when a deprecated command is used
    deprecationWarnings: true,
    //
    // If you only want to run your tests until a specific amount of tests have failed use
    // bail (default is 0 - don't bail, run all tests).
    bail: 0,
    //
    // Saves a screenshot to a given path if a command fails.
  //  screenshotPath: './error-shots/',
    //
    // Set a base URL in order to shorten url command calls. If your `url` parameter starts
    // with `/`, the base url gets prepended, not including the path portion of your baseUrl.
    // If your `url` parameter starts without a scheme or `/` (like `some/path`), the base url
    // gets prepended directly.
    baseUrl: 'https://www.google.com',
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 90000,
    //
    // Default request retries count
    connectionRetryCount: 1,
    //
    // Initialize the browser instance with a WebdriverIO plugin. The object should have the
    // plugin name as key and the desired plugin options as properties. Make sure you have
    // the plugin installed before running any tests. The following plugins are currently
    // available:
    // WebdriverCSS: https://github.com/webdriverio/webdrivercss
    // WebdriverRTC: https://github.com/webdriverio/webdriverrtc
    // Browserevent: https://github.com/webdriverio/browserevent
    // plugins: {
    //     webdrivercss: {
    //         screenshotRoot: 'my-shots',
    //         failedComparisonsRoot: 'diffs',
    //         misMatchTolerance: 0.05,
    //         screenWidth: [320,480,640,1024]
    //     },
    //     webdriverrtc: {},
    //     browserevent: {}
    // },
    //
    // Test runner services
    // Services take over a specific job you don't want to take care of. They enhance
    // your test setup with almost no effort. Unlike plugins, they don't add new
    // commands. Instead, they hook themselves up into the test process.
    // services: [],//
    // Framework you want to run your specs with.
    // The following are supported: Mocha, Jasmine, and Cucumber
    // see also: http://webdriver.io/guide/testrunner/frameworks.html
    //
    // Make sure you have the wdio adapter package for the specific framework installed
    // before running any tests.
    framework: 'cucumber',
    //
    // Test reporter for stdout.
    // The only one supported by default is 'dot'
    // see also: http://webdriver.io/guide/reporters/dot.html
   // reporters: ['cucumber'],//
    // If you are using Cucumber you need to specify the location of your step definitions.
    cucumberOpts: {
        compiler: ['ts:ts-node/register'],  
        backtrace: true,
        failFast: false,
        timeout: 5 * 60 * 60000,     // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
         require: ['./stepDefinitions/testurl.ts'],        // <string[]> (file/dir) require files before executing features
          },
    
        onPrepare: function () {
            console.log('<<< NATIVE APP TESTS STARTED >>>');
        },
    
        afterScenario: function (scenario) {
         //   browser.screenshot();
         },
    
        onComplete: function () {
            console.log('<<< TESTING FINISHED >>>');
        },


    /**
     * Gets executed before test execution begins. At this point you can access to all global
     * variables like `browser`.
     */
 /*  before: () => {
        /**
         * Setup the Chai assertion framework
         */
     /*   global.expect = chai.expect;
        global.assert = chai.assert;
        global.should = chai.should();
},*/
};
    
