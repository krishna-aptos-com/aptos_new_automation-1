
export class EJViewerPageObjects {

    // selects arrow button of store
    public static get storeList() {
        return browser.element("//*[@class='Select-arrow']");
    }

    // selects the store from the Store
    public static get selectStore() {
        // return browser.element("//*[contains(text(),'Chicago, Aptos Store')]");
        return browser.element("//*[@id='react-select-2--value-item']");
    }

    // clicks on the filterButtonx
    public static get showFilterButton() {
        return browser.element('//button');
    }

    // select the textBox for entering deviceID
    public static get deviceIDTextbox() {
        return browser.element("//input[@placeholder='Enter Device ID...']");
    }
    // select the textBox for entering deviceID
    public static get storeTextbox() {
        return browser.element("//input[@id='retailLocationId']");
    }

    // select the textBox for entering deviceID
    public static get ejViewerLabel() {
        return browser.element("//*[@class='navbar-brand']");
    }
}
