

class LoginScreen 
{
    private get username_edit_box()
    {
        return $('#username');  //by id
    }

    private get password_edit_box()
    {
        return $('#password');
    }

    private get login_btn()
    {
        return  $('~Log in');  // by accessibility id
    }

    private get invalid_credential_message ()
    {
        return $('~Invalid username or password.');
    }

    private setUsername(username:string)
    {

        this.username_edit_box.setValue(username);
    }

    private setPassword(password:string)
    {
        this.password_edit_box.setValue(password);
    }

    private clickOnLogin()
    {
        this.login_btn.click();
    }

    public userLogin(username:string, password:string)
    {
        this.setUsername(username);
        this.setPassword(password);
        this.clickOnLogin();
    }

    public InvalidCredentialMessageIsDisplayed ():boolean
    {

        return this.invalid_credential_message.isExisting();
    }

    public InvalidCredentialMessageGetText ():string
    {
        return this.invalid_credential_message.getText();
    }


}

const Login = new LoginScreen();

export default Login;