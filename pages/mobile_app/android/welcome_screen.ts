

class WelcomeScreen {

    private get welcome_msg_text ()
    {
        return $(`android=new UiSelector().textContains("Welcome")`);
    }

    public welcomeMessageText()
    {
        return this.welcome_msg_text.getText();
    }

    public isWelcomeMessageDisplayed():boolean
    {
        return this.welcome_msg_text.isExisting();
    }



}

const Welcome = new WelcomeScreen();

export default Welcome;