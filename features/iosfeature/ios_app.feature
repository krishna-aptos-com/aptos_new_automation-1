Feature: Aptos Store Selling App

Scenario Outline: Invalid credentials scenario
Given User opens Aptos Store Selling iOS App and provided the Test Environment for Platform Domain as <platformDomain> and clicks on Done button
When User provides invalid credentials with <username> and <password> and clicks on Log in button
Then User should not be logged and should prompt an invalid username and password error message
Examples:

             | platformDomain                 | username | password     |
             | test.aptos-denim.aptos-labs.io | invalid username  | invalid password |

                    # Add invalid username

Scenario Outline: Valid credentials scenario
Given User opens Aptos Store Selling iOS App and provided the Test Environment for Platform Domain as <platformDomain> and clicks on Done button
When User provides valid credentials with <username> and <password> and clicks on Log in button
Then should be able login successfully

Examples:

               | platformDomain                 | username | password |
               | test.aptos-denim.aptos-labs.io | valid username | valid password|

# Add valid credentials

Scenario: Validate Store Associate Settings page
Given User Logged in successfully and verify the Setting page
When User select Chicago retail Store and provides the Terminal Number as 145_0001 and click on next transaction Number
Then User should be able to redirect to Store Associate Login page

Scenario Outline: Validate Store Associate Login with Invalid credentials
When User provide Invalid Store Associate credentials as <username> and <password>
Then User should not able to login and should show invalid username or password alert popup

Examples:

                | username | password |
                | 2222     | 3333333  |

Scenario Outline: Validate Store Associate Login with valid credentials
When User provide valid Store Associate credentials as <username> and <password>
Then User should be logged in with logged in username gretting message

Examples:

                | username | password |
                | 2222     | 2222222  |
