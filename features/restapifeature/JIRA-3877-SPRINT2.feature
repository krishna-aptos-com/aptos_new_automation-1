Feature: Create a new user

  Background:
  Scenario Outline:  Generating access token

    Given post url "<URL>" request with input body is

    """
     {
    "grant_type":"password",
    "username":"swati.shikhar",
    "password":"Zensar@123",
    "client_id":"localhost-dev"
     }

    """
    Then the status code must be "<statuscode>"

  @dev_env
    Examples:
      |URL                                                                                                    |statuscode|
      |https://idm.dev.aptos-denim.aptos-labs.io/auth/realms/aptos-denim_default/protocol/openid-connect/token|200       |
  @qa_env
    Examples:
      |URL                                                                                                    |statuscode|
      |https://idm.test.aptos-denim.aptos-labs.io/auth/realms/aptos-denim_default/protocol/openid-connect/token|200       |



  Scenario Outline: C10 creating new user

    When POST a url "<URL>" request with input body is
    """
    {
    "username": "kris123418",
    "password": "Aptos@1234",
    "firstName": "Krishna",
    "lastName": "Deergasi",
    "accountStatus": "Active",
    "phoneNumbers": [
    {
    "typeCode": "home",
    "phoneNumber": "122-333-333"
    }
    ]
    }
    """

  Then the status code must be "<statuscode>"
  @dev_env
  Examples:
  |URL                                                     |statuscode|
  |https://api.dev.aptos-denim.aptos-labs.io/users/v2/users|200       |
  @qa_env
  Examples:
  |URL                                                      |statuscode|
  |https://api.test.aptos-denim.aptos-labs.io/users/v2/users|200       |


  Scenario Outline: : Get Users List params

    When send a GET request to url "<URL>" request with dynamic userid
    Then the status code must be "<statuscode>"
    Then response includes the following in any order
      | data[*].username                                                         | 1111,"testuser182436",1221 |
      | data[*].phoneNumbers[*].phoneNumber                                      | 122-222-2222         |
  | data[*].retailLocationRoles[*].securityRoleId                            | 10                    |
  @dev_env
    Examples:
      |URL                                                            |statuscode|
      |https://api.dev.aptos-denim.aptos-labs.io/users/v2/users/userid|200       |
