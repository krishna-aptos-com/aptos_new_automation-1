Feature: Get User List

  Background:
  Scenario Outline: C3 Generating access token

    Given post url "<URL>" request with input body is

    """
     {
    "grant_type":"password",
    "username":"swati.shikhar",
    "password":"Zensar@123",
    "client_id":"localhost-dev"
     }

    """
    Then the status code must be "<statuscode>"

    @dev_env
    Examples:
      |URL                                                                                                    |statuscode|
      |https://idm.dev.aptos-denim.aptos-labs.io/auth/realms/aptos-denim_default/protocol/openid-connect/token|200       |
    @qa_env
    Examples:
      |URL                                                                                                    |statuscode|
      |https://idm.test.aptos-denim.aptos-labs.io/auth/realms/aptos-denim_default/protocol/openid-connect/token|200       |

  @Regression
  Scenario Outline: C4 Get particular user from users list

    When send GET request to url "<URL>"
    Then the status code must be "<statuscode>"
    Then user response includes the following
        | username                                                                 | 1111                 |
        | phoneNumber                                                              | 122-222-2222         |
        |securityRoleId                                                            | 10                   |
        |userId                                                                    | cdcd8598-46f2-4bec-8167-de3b800b0bde|
    @dev_env
    Examples:
      |URL                                                                                                    |statuscode|
      |https://api.dev.aptos-denim.aptos-labs.io/users/v2/users/cdcd8598-46f2-4bec-8167-de3b800b0bde          |200       |

  Scenario Outline: C5 Get users list with limit

    When send GET request to url "<URL>"
    Then the status code must be "<statuscode>"
    Then response includes the following in any order
      | data[*].username                                                         | 1111,"testuser182436",1221                 |
      | data[0].phoneNumbers[1].phoneNumber                                      | 122-222-2222         |
      | data[*].retailLocationRoles[*].securityRoleId                            | 10                    |
  @dev_env
    Examples:
      |URL                                                                               |statuscode|
      |https://api.dev.aptos-denim.aptos-labs.io/users/v2/users?limit=5&offset=0         |200       |

  Scenario Outline: C6 Get users list with limit should not exceed 5 as well check given user not present

    When send GET request to url "<URL>"
    Then the status code must be "<statuscode>"
    Then response not includes the following in any order
      | data[*].username                                                         |   1226              |
      | data[5].username                                                         |                 |

  @dev_env
    Examples:
      |URL                                                                               |statuscode|
      |https://api.dev.aptos-denim.aptos-labs.io/users/v2/users?limit=5&offset=0         |200       |


Scenario Outline: : Get Users List params

  When send a GET request to url "<URL>" request with dynamic userid
  Then the status code must be "<statuscode>"
  Then response includes the following in any order
    | data[*].username                                                         | 1111,"testuser182436",1221 |
    | data[*].phoneNumbers[*].phoneNumber                                      | 122-222-2222         |
    | data[*].retailLocationRoles[*].securityRoleId                            | 10                    |
@dev_env
  Examples:
    |URL                                                            |statuscode|
    |https://api.dev.aptos-denim.aptos-labs.io/users/v2/users/userid|200       |
